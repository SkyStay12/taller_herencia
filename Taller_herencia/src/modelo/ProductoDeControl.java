package modelo;
/**
 *
 * @author Alejandra Lopez
 */
public class ProductoDeControl {
    private String ica;
    private String nombreProducto;
    private String frecuenciaAplicacion;

    public ProductoDeControl(){
    }

    public ProductoDeControl(String ica) {
        this.ica = ica;
    }

    public ProductoDeControl(String ica, String nombreProducto, String frecuenciaAplicacion) {
        this.ica = ica;
        this.nombreProducto = nombreProducto;
        this.frecuenciaAplicacion = frecuenciaAplicacion;
    }
    
    
    
    public String getIca() {  
        return ica;
    }

    public void setIca(String ica) {
        this.ica = ica;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getFrecuenciaAplicacion() {
        return frecuenciaAplicacion;
    }

    public void setFrecuenciaAplicacion(String frecuenciaAplicacion) {
        this.frecuenciaAplicacion = frecuenciaAplicacion;
    }

    @Override
    public String toString() {
        return "ProductoDeControl{" + "ica=" + ica + ", nombreProducto=" + nombreProducto + ", frecuenciaAplicacion=" + frecuenciaAplicacion + '}';
    }
    
}
