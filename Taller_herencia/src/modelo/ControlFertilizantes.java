package modelo;
import java.time.LocalDate;
/**
 *
 * @author Alejandra Lopez
 */
public class ControlFertilizantes extends ProductoDeControl {
       
    private LocalDate fechaUltimaAplicacion;

    public ControlFertilizantes(LocalDate fechaUltimaAplicacion, String ica) {
        super(ica);
        this.fechaUltimaAplicacion = fechaUltimaAplicacion;
    }
        
 
    public ControlFertilizantes(String fecha, String ica, String nombreProducto, String frecuenciaAplicacion) {
        super(ica, nombreProducto, frecuenciaAplicacion);
    //La fecha debe ingresarse de esta manera "2019-03-23"
        this.fechaUltimaAplicacion = LocalDate.parse(fecha);
    }

    public LocalDate getFechaUltimaAplicacion() {
        return fechaUltimaAplicacion;
    }

    public void setFechaUltimaAplicacion(LocalDate fechaUltimaAplicacion) {
        this.fechaUltimaAplicacion = fechaUltimaAplicacion;
    }

    @Override
    public String toString() {
        return super.toString() + "ControlFertilizantes{" + "fechaUltimaAplicacion=" + fechaUltimaAplicacion + '}';
    }

    
        
        
}
