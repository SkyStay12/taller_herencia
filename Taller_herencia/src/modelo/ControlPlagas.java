package modelo;
/**
 *
 * @author Alejandra Lopez
 */
public class ControlPlagas extends ProductoDeControl {
    private String periodoCarencia;

    public ControlPlagas(String periodoCarencia, String ica) {
        super(ica);
        this.periodoCarencia = periodoCarencia;
    }

    public ControlPlagas(String periodoCarencia, String ica, String nombreProducto, String frecuenciaAplicacion) {
        super(ica, nombreProducto, frecuenciaAplicacion);
        this.periodoCarencia = periodoCarencia;
    }

  
    public String getPeriodoCarencia() {
        return periodoCarencia;
    }

    public void setPeriodoCarencia(String periodoCarencia) {
        this.periodoCarencia = periodoCarencia;
    }
    
    
    @Override
    public String toString() {
        return super.toString() +"ControlPlagas{" + "periodoCarencia=" + periodoCarencia + '}';
    }
}
